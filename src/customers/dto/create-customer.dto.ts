import { IsNotEmpty, IsPositive, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsPositive()
  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  gender: string;
}
